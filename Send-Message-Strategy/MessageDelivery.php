<?php

class MessageDelivery
{
    private $message;
    private $deliveryType;


    public function __construct(ServiceType $deliveryType, string $message)
    {
        if (empty($message)) {
            $this->errorExit();
        } else {
            $this->message = $message;
            $this->deliveryType = $deliveryType;
        }
    }


    public function setDeliveryType(ServiceType $deliveryType, string $message)
    {
        if (empty($message)) {
            $this->errorExit();
        } else {
            $this->message = $message;
            $this->deliveryType = $deliveryType;
        }
    }

    public function makeMessageDelivery()
    {
        return $this->deliveryType->sendMessage($this->message);
    }

    private function errorExit()
    {
        echo 'Something went wrong. Please try again later.';
        exit();
    }
}
