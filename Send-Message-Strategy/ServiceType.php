<?php

interface ServiceType
{
    public function sendMessage(string $message);
}
