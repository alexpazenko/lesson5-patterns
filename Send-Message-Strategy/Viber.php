<?php

class Viber implements ServiceType
{
    public function sendMessage(string $message)
    {
        return "We have sent a message (".$message.") to you in Viber. Please check your Viber account.";
    }

}