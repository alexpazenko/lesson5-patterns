<?php


class Email implements ServiceType
{
    public function sendMessage(string $message)
    {
        return "We've sent a message (".$message.") to your email. Please check your email.";
    }

}
