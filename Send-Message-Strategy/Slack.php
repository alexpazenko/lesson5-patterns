<?php

class Slack implements ServiceType
{
    public function sendMessage(string $message)
    {
        return "We have sent a message (".$message.") to you in Slack. Please check your Slack account.";
    }

}
