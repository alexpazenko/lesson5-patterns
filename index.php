<?php
require_once 'Singleton/DBConnection.php';
require_once 'Singleton/Users.php';
require_once 'Strategy-Navigator/GetPlace.php';
require_once 'Strategy-Navigator/Navigator.php';
require_once 'Strategy-Navigator/Car.php';
require_once 'Strategy-Navigator/Bus.php';
require_once 'Strategy-Navigator/Walk.php';

require_once 'PaymentTypeStrategy/PaymentType.php';
require_once 'PaymentTypeStrategy/AfterDelivery.php';
require_once 'PaymentTypeStrategy/CreditСard.php';
require_once 'PaymentTypeStrategy/PaymentAccountOnSite.php';
require_once 'PaymentTypeStrategy/Payment.php';

require_once 'Send-Message-Strategy/ServiceType.php';
require_once 'Send-Message-Strategy/Email.php';
require_once 'Send-Message-Strategy/Slack.php';
require_once 'Send-Message-Strategy/Viber.php';
require_once 'Send-Message-Strategy/MessageDelivery.php';


$users = new Users();
$allUsers = $users->getAllUsers();

foreach ($allUsers as $name)
{
    echo $name['Name'] . '<br>';
}

$firstUser = new Users();
$userName = $firstUser->getUserWithId(1);
echo $userName . '<br>';



$navigator = new Navigator(803, new Car());
echo $navigator->calculate() . '<br>';

$navigator = new Navigator(177, new Bus());
echo $navigator->calculate() . '<br>';

$navigator->setGetBy(551, new Walk());
echo $navigator->calculate(). '<br>';


$payment = new Payment(350, new CreditСard());
echo $payment->makePayment() . '<br>';

$payment->setPayBy(1450, new AfterDelivery());
echo $payment->makePayment() . '<br>';

$payment->setPayBy(740, new PaymentAccountOnSite());
echo $payment->makePayment() . '<br>';


$messageDelivery = new MessageDelivery(new Email(), 'Hello!');
echo $messageDelivery->makeMessageDelivery() . '<br>';

$messageDelivery->setDeliveryType(new Slack(), 'Hello!');
echo $messageDelivery->makeMessageDelivery() . '<br>';

$messageDelivery->setDeliveryType(new Viber(), 'Hello!');
echo $messageDelivery->makeMessageDelivery() . '<br>';