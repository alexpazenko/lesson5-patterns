<?php

class CreditСard implements PaymentType
{
    public function pay(int $amount)
    {
        return 'The amount ('.$amount.') has been debited from your Credit Card. Thank you!';
    }

}