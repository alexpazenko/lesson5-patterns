<?php

class Payment
{
    private $payBy;
    private $paymentAmount;


    public function __construct(int $paymentAmount, PaymentType $payBy)
    {
        if ($paymentAmount <= 0) {
            $this->errorExit();
        } else {
            $this->paymentAmount = $paymentAmount;
            $this->payBy = $payBy;
        }
    }


    public function setPayBy(int $paymentAmount, PaymentType $payBy)
    {
        if ($paymentAmount <= 0) {
            $this->errorExit();
        } else {
        $this->paymentAmount = $paymentAmount;
        $this->payBy = $payBy;
        }
    }

    public function makePayment()
    {
        return $this->payBy->pay($this->paymentAmount);
    }

    private function errorExit()
    {
        echo 'Something went wrong. Please try again later.';
        exit();
    }
}