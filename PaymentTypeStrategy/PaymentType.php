<?php

interface PaymentType
{
    public function pay(int $amount);
}