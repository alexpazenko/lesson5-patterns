<?php

class PaymentAccountOnSite implements PaymentType
{
    public function pay(int $amount)
    {
        return 'The amount ('.$amount.') has been debited from your payment account on the site. Thank you!';
    }

}