<?php

class AfterDelivery implements PaymentType
{
    public function pay(int $amount)
    {
        return 'You will pay the amount of ('.$amount.') after you receive the product. Thank you!';
    }

}