<?php


class DBConnection
{
    private static $instance = null;
    private static $connection;

    private static $host = 'database';
    private static $dbName = 'lesson5-patterns';
    private static $user = 'user';
    private static $password = 'user';

    private function __construct()
    {
        try {
            self::$connection = new PDO("mysql:host=".self::$host.";dbname=".self::$dbName, self::$user, self::$password);
        } catch (PDOException $error) {
            print "Error!: " . $error->getMessage() . "<br/>";
            die();
        }
    }

    public static function getInstance(): DBConnection
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __clone()
    {
    }

        private function __wakeup()
    {
    }

    public static function query($sql)
    {
        $sth = self::$connection->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
}