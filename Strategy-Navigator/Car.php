<?php

class Car implements GetPlace
{
    private $averageSpeed = 80;
    public function calculateTime(int $distance)
    {
        return ($distance/$this->averageSpeed)*3600;

    }
}