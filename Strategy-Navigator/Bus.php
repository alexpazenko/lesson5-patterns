<?php

class Bus implements GetPlace
{
    private $averageSpeed = 65;
    public function calculateTime(int $distance)
    {
        return ($distance/$this->averageSpeed)*3600;
    }
}