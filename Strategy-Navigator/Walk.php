<?php

class Walk implements GetPlace
{
    private $averageSpeed = 5;
    public function calculateTime(int $distance)
    {
        return ($distance/$this->averageSpeed)*3600;
    }

}