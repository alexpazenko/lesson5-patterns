<?php

class Navigator
{
    private $getBy;
    private $distance;


    public function __construct(int $distance, GetPlace $getBy)
    {
        if ($distance <= 0) {
            $this->errorExit();
        } else {
            $this->distance = $distance;
            $this->getBy = $getBy;
        }
    }

    public function setGetBy(int $distance, GetPlace $getBy)
    {
        if ($distance <= 0) {
            $this->errorExit();
        } else {
            $this->distance = $distance;
            $this->getBy = $getBy;
        }
    }
    public function calculate()
    {
        return $this->secondsToTime($this->getBy->calculateTime($this->distance));
    }
    private function secondsToTime(int $seconds) {
        if ($seconds >= 86400) {
            $timeFormat = '%a days, %h hours, %i minutes and %s seconds';
        } elseif ($seconds < 86400 && $seconds >= 3600 ) {
            $timeFormat = '%h hours, %i minutes and %s seconds';
        } elseif ($seconds < 3600 && $seconds >= 60 ) {
            $timeFormat = '%i minutes and %s seconds';
        } elseif ($seconds < 60 ) {
            $timeFormat = '%s seconds';
        }
        $dtF = new DateTime('@0');
        $dtT = new DateTime("@$seconds");
        return $dtF->diff($dtT)->format($timeFormat);
    }

    private function errorExit()
    {
        echo 'Something went wrong. Please try again later.';
        exit();
    }
}