<?php

interface GetPlace
{
    public function calculateTime(int $distance);
}